package com.barber.bateria;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.barber.bateria.ui.login.LoginActivity;

public class splash extends AppCompatActivity {


    static int TIMEOUT_MILLIS =5000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getSupportActionBar().hide();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
            Intent i = new Intent(splash.this, LoginActivity.class);
            startActivity(i);
            finish();
        }
    },TIMEOUT_MILLIS);
}
}